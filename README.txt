setup:

developed on Ubuntu 16.04.6 for ros-kinetic

install dependencies: 
sudo apt-get install ros-kinetic-desktop-full (check http://wiki.ros.org/kinetic/Installation/Ubuntu for detailed instruction)
sudo apt-get install ros-kinetic-universal-robot
sudo apt-get install ros-kinetic-soem
sudo apt-get install ros-kinetic-usb-cam
sudo apt-get install ros-kinetic-rosserial
sudo apt-get install ros-kinetic-rosserial-arduino 


get acces to USB port for robotiq control:
sudo usermod -a -G tty,dialout $USER        (replace $USER with username)

To calibrate camera:
sudo apt-get install ros-kinetic-camera-calibration
roscore
rosrun usb_cam usb_cam_node _pixel_format:=yuyv
rosrun camera_calibration cameracalibrator.py --size 8x6 --square __SIZE_OF_CHECKERBOARD_SQUARE_SIZE__ image:=/usb_cam/image_raw camera:=/usb_cam


Changing the type of 'wrist_3_joint' from revolute to continuous on line 255, and removing line 260-265 of /opt/ros/kinetic/share/ur_description/urdf/ur3.urdf.xacro might improve planning performance of moveit