#!/usr/bin/env python
from __future__ import print_function

import sys
import rospy
import cv2
from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

class image_converter:

    def __init__(self):
        self.image_pub = rospy.Publisher("image_topic",Image)

        self.bridge = CvBridge()
        self.image_sub = rospy.Subscriber("usb_cam/image_rect",Image,self.callback)

    def callback(self,data):
        try:
            cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
        except CvBridgeError as e:
            print(e)

        (rows,cols,channels) = cv_image.shape
        if cols > 60 and rows > 60 :
            redChan = 	cv2.extractChannel(cv_image, 2)
            redChan = cv2.blur(redChan, (5,5))
            _, maxVal, _, maxLoc = cv2.minMaxLoc(redChan)
            print("brightest:\tx:%d\ty:%d\tval:%d" % (maxLoc[0], maxLoc[1],maxVal))
            #cv2.circle(redChan, maxLoc, 30, 255, 5)
            ret, thresh = cv2.threshold(redChan, 100, 255, cv2.THRESH_BINARY)
            im2, contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
            cv2.drawContours(cv_image, contours, -1, (0,255,0), 3)
            print(type(contours))
            if (len(contours) > 0):
                M = cv2.moments(contours[0])
                #print(M)
                cX = int(M["m10"] / M["m00"])
                cY = int(M["m01"] / M["m00"])
                print("moment:\t\tx:%d\ty:%d" % (cX, cY))
                cv2.circle(cv_image, (cX, cY), 7, (255, 0, 0), -1)

        cv2.imshow("Image window", cv_image)
        cv2.waitKey(3)

        try:
            self.image_pub.publish(self.bridge.cv2_to_imgmsg(cv_image, "bgr8"))
        except CvBridgeError as e:
            print(e)

def main(args):
    ic = image_converter()
    rospy.init_node('image_converter', anonymous=True)
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main(sys.argv)
