#!/usr/bin/env python

import rospy, sys, numpy as np
import roslib; roslib.load_manifest('ur_driver')
import actionlib
from control_msgs.msg import *
from trajectory_msgs.msg import *
from sensor_msgs.msg import JointState
from math import pi

import moveit_commander
from copy import deepcopy
import geometry_msgs.msg

from myrobot.msg import finPoint

import tf.transformations

import moveit_msgs.msg
from std_msgs.msg import Header
from robotiq_2f_gripper_control.msg import *


def test():
    moveit_commander.roscpp_initialize(sys.argv)
    rospy.init_node('test')
    robot = moveit_commander.RobotCommander()
    scene = moveit_commander.PlanningSceneInterface()
    group = moveit_commander.MoveGroupCommander("manipulator")


    while not rospy.is_shutdown():
        pose_goal = group.get_current_pose().pose
        print("x:%d\ty:%d\tz:%d\t" %(pose_goal.position.x, pose_goal.position.y, pose_goal.position.z))
        rospy.sleep(0.2)
        

if __name__ == '__main__':
    try:
        test()
    except rospy.ROSInterruptException:
        pass
