#!/usr/bin/env python
import rospy
from myrobot.msg import finPoint


#from sensor_msgs.msg import Image

def talker():
    fp =finPoint()
    pub = rospy.Publisher('cam_coord', finPoint, queue_size=10)
    rospy.init_node('sim_cam')#, anonymous=True)
    rate = rospy.Rate(1) # 1hz
    while not rospy.is_shutdown():
        fp.x = 0.3
        fp.y = 0.3
        fp.valid = True
        rospy.loginfo(fp)
        pub.publish(fp)
        rate.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
