#!/usr/bin/env python


import rospy, sys, numpy as np
import roslib; roslib.load_manifest('ur_driver')
import actionlib
from control_msgs.msg import *
from trajectory_msgs.msg import *
from sensor_msgs.msg import JointState
from math import pi

import moveit_commander
from copy import deepcopy
import geometry_msgs.msg

from myrobot.msg import *
from tf.transformations import *

import moveit_msgs.msg
from std_msgs.msg import Header
from robotiq_2f_gripper_control.msg import *

class myrobot_mp:
    def __init__(self):
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node('myrobot_mp')

        rospy.Subscriber('cam_coord', finPoint, self.coordCallback)
        rospy.Subscriber('Robotiq2FGripperRobotInput', Robotiq2FGripper_robot_input, self.gripperCallback)
        rospy.Subscriber('drone_out', drone_status, self.droneCallback)

        self.r2f_pub = rospy.Publisher('Robotiq2FGripperRobotOutput', Robotiq2FGripper_robot_output, queue_size=10)
        self.r2f_out = Robotiq2FGripper_robot_output()
        self.r2f_input = Robotiq2FGripper_robot_input()
        self.idle = True
        self.x = 0
        self.y = 0
        self.status = False #drone status -> True if landed
        self.valid = False #camera found bright spot -> True if found
        self.robot = moveit_commander.RobotCommander()
        self.scene = moveit_commander.PlanningSceneInterface()
        self.group = moveit_commander.MoveGroupCommander("manipulator")
        #self.client = actionlib.SimpleActionClient('follow_joint_trajectory', FollowJointTrajectoryAction)
        self.group_names = self.robot.get_group_names()

        self.display_trajectory_publisher = rospy.Publisher('/move_group/display_planned_path',
                                                   moveit_msgs.msg.DisplayTrajectory,
                                                   queue_size=20)

        #print("%s" %(self.group.get_end_effector_link()))
        rospy.sleep(1)

        #add table to scene model for collision avoidance
        t = geometry_msgs.msg.PoseStamped()
        t.header.frame_id = self.robot.get_planning_frame()
        t.pose.position.x = 0.
        t.pose.position.y = 0.
        t.pose.position.z = -0.051
        #print("adding box")
        self.scene.add_box("table", t, (2,2,0.1))
        

        #add charging to scene model for collision avoidance
        c = geometry_msgs.msg.PoseStamped()
        c.header.frame_id = self.robot.get_planning_frame()
        c.pose.position.x = -0.45
        c.pose.position.y = 0.17
        c.pose.position.z = 0.065
        #print("adding box")
        self.scene.add_box("chargingStation", c, (0.5,0.6,0.13))
        

        #TODO: Initialize gripper!
        self.r2f_pub.publish(self.r2f_out) #reset
        rospy.sleep(0.3)
        self.r2f_out.rACT = 1
        self.r2f_out.rGTO = 1
        self.r2f_out.rSP = 255
        self.r2f_out.rFR = 150
        self.r2f_pub.publish(self.r2f_out) #activate
        rospy.sleep(1)

    def droneCallback(self, msg):
        self.heading = msg.heading
        self.status = msg.status
    
    def coordCallback(self, fp):
        self.x = fp.x
        self.y = fp.y
        self.valid = fp.valid

    def gripperCallback(self, msg):    
        self.r2f_input.gACT = msg.gACT
        self.r2f_input.gGTO = msg.gGTO
        self.r2f_input.gSTA = msg.gSTA 
        self.r2f_input.gOBJ = msg.gOBJ
        self.r2f_input.gFLT = msg.gFLT
        self.r2f_input.gPR = msg.gPR
        self.r2f_input.gPO = msg.gPO
        self.r2f_input.gCU = msg.gCU

    def execute(self):
        self.group.allow_replanning(True)

        # Allow some leeway in position (meters) and orientation (radians)
        self.group.set_goal_position_tolerance(0.002)
        self.group.set_goal_orientation_tolerance(0.02)
        self.group.set_planning_time(3)
        self.group.set_num_planning_attempts(50)
        self.group.set_max_acceleration_scaling_factor(.1)
        self.group.set_max_velocity_scaling_factor(.1)
        

        P1 = [-1.92, -2.06, -0.585, -0.505, 1.733, 0]

        if (not self.group.go(P1, wait=True)): #stand-by position
            print("No path found! Stand-by position")
            return

        
        #set orientation
        
        while not((self.valid and self.idle and self.status) or rospy.is_shutdown()):
            print("waiting for valid")
            rospy.sleep(1)

        
        self.idle = False

        pose_goal = geometry_msgs.msg.Pose()
        pose_goal.position.x = self.x
        pose_goal.position.y = self.y
        pose_goal.position.z = 0.29

        #rotate to drone heading
        #self.heading -> [0-255] 0 is along the arm y-axis
        if self.heading > 128:
            radOff = (0-((self.heading*2*np.pi)/256)) % np.pi * -1
        elif self.heading == 128:
            radOff = np.pi
        else:
            radOff = ((self.heading*2*np.pi)/256) % np.pi
        #print(radOff)
        q_down = [1, 0, 0, 0]
        q_rot = quaternion_from_euler(0, 0, radOff)
        q_new = quaternion_multiply(q_rot, q_down)
        #print(q_new)
        pose_goal.orientation.x = q_new[0]
        pose_goal.orientation.y = q_new[1]
        pose_goal.orientation.z = q_new[2]
        pose_goal.orientation.w = q_new[3]
        self.group.set_pose_target(pose_goal)
        
        if (not self.group.go( wait=True)): #above pick-up-point
            print("No path found! Above pick-up point")
            return
        
        self.group.stop()
        self.group.clear_pose_targets()


        pose_goal.position.z = 0.25
        self.group.set_pose_target(pose_goal)
        
        if (not self.group.go( wait=True)): #above pick-up-point
            print("No path found! Pick-up point")
            return
        
        self.group.stop()
        self.group.clear_pose_targets()
        
        
        #add drone to scene
        box_pose = geometry_msgs.msg.PoseStamped()
        box_pose.header.frame_id = self.robot.get_planning_frame()
        box_pose.pose.position.x = self.x
        box_pose.pose.position.y = self.y
        box_pose.pose.position.z = 0.0425
        box_pose.pose.orientation = pose_goal.orientation
        box_name = "drone"
        self.scene.add_box(box_name, box_pose, size=(0.185, 0.16, 0.085))

        self.r2f_out.rPR = 225
        self.r2f_pub.publish(self.r2f_out)
        #print("picking up")
        
        #wait for gripper to close
        while (self.r2f_input.gPO > self.r2f_out.rPR + 15) or (self.r2f_input.gPO < self.r2f_out.rPR - 15):
            rospy.sleep(0.3)

        #attach box to gripper for moveit planning
        touch_links = self.robot.get_link_names(group="gripper")
        self.scene.attach_box(self.group.get_end_effector_link(), box_name, touch_links=touch_links)

        rospy.sleep(0.2)

        pose_goal.position.z = 0.29
        self.group.set_pose_target(pose_goal)
        if (not self.group.go( wait=True)): #above pick-up-point
            print("No path found! Pick-up point")
            return
        self.group.stop()
        self.group.clear_pose_targets()

        #end point - drone charging station
        pose_goal.position.x = -0.414
        pose_goal.position.y = 0.007
        pose_goal.position.z = 0.39 #temporaryly increased for card board box
        #pose_goal.position.z = 0.35
        pose_goal.orientation.x = np.sqrt(0.5)
        pose_goal.orientation.y = np.sqrt(0.5)

        self.group.set_pose_target(pose_goal)
        print("trying to move above drop-off-point")
        if (not self.group.go(wait=True)): #above charging station
            print("No path found! Above drop off")
            return
        self.group.stop()
        self.group.clear_pose_targets()

        """ #temporaryly disabled for card board box
        pose_goal.position.z = 0.335
        self.group.set_pose_target(pose_goal)
        if (not self.group.go(wait=True)): #above charging station
            print("No path found! Lowering to drop off")
            return
        self.group.stop()
        self.group.clear_pose_targets()
        """
        
        #print("dropping drone")
        self.r2f_out.rPR = 0
        self.r2f_pub.publish(self.r2f_out)
        while (self.r2f_input.gPO > self.r2f_out.rPR + 5) or (self.r2f_input.gPO < self.r2f_out.rPR - 5) and not rospy.is_shutdown():
            rospy.sleep(0.3)
        
        self.scene.remove_attached_object(self.group.get_end_effector_link(), name=box_name)
        self.scene.remove_world_object(box_name)
            
        self.idle = True

        rospy.sleep(0.3)



if __name__ == '__main__':
    try:
        myrob = myrobot_mp()
        myrob.execute()
    except rospy.ROSInterruptException:
        pass
