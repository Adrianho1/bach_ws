#!/usr/bin/env python
from __future__ import print_function

import sys
import rospy
import cv2
import numpy as np
from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
from myrobot.msg import finPoint

class image_converter:

    def __init__(self):
        rospy.init_node('cam_node')
        self.coord_pub = rospy.Publisher("cam_coord",finPoint , queue_size=5)

        self.bridge = CvBridge()
        self.image_sub = rospy.Subscriber("usb_cam/image_rect",Image,self.callback)

    def callback(self,data):
        try:
            cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
        except CvBridgeError as e:
            print(e)
			
		threshVal = 100 #TUNE THIS
			
        cX = cY = 0
        (rows,cols,channels) = cv_image.shape
        
        cameraScale = 0.217/640

        #coordinate transform:
        #rotation matrices:
        rotY = [[np.cos(np.pi),0,np.sin(np.pi)], [0, 1, 0], [-np.sin(np.pi), 0 ,np.cos(np.pi)]]
        rotZ = [[np.cos(-71*np.pi/180),-np.sin(-71*np.pi/180), 0], [np.sin(-71*np.pi/180), np.cos(-71*np.pi/180), 0], [0, 0, 1]]
        rot = np.dot(rotY, rotZ)

        #translation
        displacement = [[0.359],[0.297], [0]]
        translationMat = np.concatenate((rot, displacement),1)
        translationMat = np.concatenate((translationMat, [[0,0,0,1]]),0)
        

        if cols > 60 and rows > 60:
            redChan = 	cv2.extractChannel(cv_image, 2)
            redChan =  cv2.blur(redChan, (5,5))
            _, maxVal, _, maxLoc = cv2.minMaxLoc(redChan)
            thresh = cv2.threshold(redChan, threshVal, 255, cv2.THRESH_BINARY)[1]
            contours = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[1]
            if (len(contours) > 0):
				contours = max(contours, key = cv2.contourArea)
                M = cv2.moments(contours[0])
                cX = int(M["m10"] / M["m00"])
                cY = int(M["m01"] / M["m00"])

                coord = np.dot(translationMat, [[cX*cameraScale],[cY*cameraScale], [0], [1]])
                outMsg = finPoint()
                outMsg.x = coord[0]
                outMsg.y = coord[1]
                if maxVal > threshVal:
                    self.coord_pub.publish(outMsg)
					
def main(args):
    ic = image_converter()
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main(sys.argv)
