#!/usr/bin/env python

import sys
import rospy
import copy

from myrobot.msg import drone_status #custom .msg in drone_emulator/msg/drone_status


def drone_emu_node ():	#name of node
	pub = rospy.Publisher('drone_out', drone_status, queue_size=10) #making publishing handle
	rospy.init_node('drone_emu_node', anonymous=False)	#initialize the node. 'False' keeps original name
	rate = rospy.Rate(10)	#Possibly remove this one. Might fuck things up.
	msg = drone_status()


	while not rospy.is_shutdown():
		print("---")

		temp_status = raw_input('Was drone landing successful? [y/n]: ')
		temp_status = temp_status.lower()

		if temp_status == ('y' or 'yes'):
			msg.status = True
		else:
			msg.status = False

		#temp_heading = input("What is the landing orientation of the drone? [0-255]: ")
		msg.heading = input("What is the landing orientation of the drone? [0-255]: ") #temp_heading
		rospy.loginfo(msg)		#logging msg.
		pub.publish(msg)		#publish msg.
		rate.sleep()			#sleep for rate

if __name__ == '__main__':
	try:
		drone_emu_node()
	except rospy.ROSInterruptException:
		pass