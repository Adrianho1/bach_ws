#include <ros.h>
#include <std_msgs/UInt8.h>
#include <std_msgs/Bool.h>
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

#define baud 9600

RF24 radio(9,10); // CE, CSN
const byte addresses[][6] = {"sNode","dNode"};
byte heading;

ros::NodeHandle nh;

std_msgs::UInt8 hdg_msg;
ros::Publisher drone_heading("drone_heading", &hdg_msg);

byte promptHeading (const std_msgs::Bool& prompt_msg){
  if(prompt_msg.data){
    heading = getHeading();
    hdg_msg.data = heading;
    Serial.println(heading);
    drone_heading.publish( &hdg_msg );  
  }
 } 
  
ros::Subscriber<std_msgs::Bool> drone_prompt("drone_prompt", &promptHeading);





/*------Setup-----------------------------------------------------------------*/
void setup() {
  initSerial();
  initNRF();
  
  nh.initNode();
  nh.advertise(drone_heading);
  nh.subscribe(drone_prompt);

}

void loop() {
  /*heading = getHeading();
  hdg_msg.data = heading;
  Serial.println(heading);
  drone_heading.publish( &hdg_msg );
  */
  nh.spinOnce();
  delay(1000);
}

/*------Initializations-------------------------------------------------------*/

void initSerial(){
  Serial.begin(baud);
}
void initNRF(){
  radio.begin();
  radio.openWritingPipe(addresses[0]);          //Write on sNode
  radio.openReadingPipe(1,addresses[1]);        //Listen on dNode
  radio.setPALevel(RF24_PA_MIN);                //Set min.TX-power
}
/*-------Functions------------------------------------------------------------*/
byte getHeading(){                                  ///Prompts drone for heading
    radio.stopListening();
    int pendingRequest = 1;
    Serial.println("Sending...");                              //Free to comment
    radio.write(&pendingRequest, sizeof(pendingRequest));
    
    radio.startListening();
    delay(20);                                                      //needs 20ms
    if (radio.available()) {
        int headingRx;
        radio.read(&headingRx, sizeof(headingRx));
        Serial.print("Drone HDG: ");                           //Free to comment
        Serial.println(headingRx);                             //Free to comment
        return headingRx;

  }
}
