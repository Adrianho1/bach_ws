

#define csMPU 8
#define csRF24 10
#define magY IMU.getMagY_uT()
#define magX IMU.getMagX_uT()
#define magZ IMU.getMagZ_uT()


#include "MPU9250.h"

// an MPU9250 object with the MPU-9250 sensor on I2C bus 0 with address 0x68

MPU9250 IMU(Wire,0x68);
int status;
float heading;

void setup() {
  pinMode(csRF24, OUTPUT);      //setting Chip Select for nRF24
  digitalWrite(csRF24, HIGH);   //disabling nRF24
  
  // serial to display data
  Serial.begin(9600);
  while(!Serial) {}

  // start communication with IMU 
  status = IMU.begin();
  if (status < 0) {
    Serial.println("IMU initialization unsuccessful");
    Serial.println("Check IMU wiring or try cycling power");
    Serial.print("Status: ");
    Serial.println(status);
    while(1) {}
  }
}

void loop() {
  // read the sensor
  IMU.readSensor();
 

  Serial.print("MagX: ");  
  Serial.print(magX,3);
  Serial.print("\t");  
  Serial.print("MagY: ");
  Serial.print(magY,3);
  Serial.print("\t");
  Serial.print("MagZ: ");  
  Serial.print(magZ,3);
  Serial.print("\t");

  heading = /*90 - */atan2(magY,magX)*(180)/PI;
  if (heading < 0){
  heading = 360 + heading;
  }
  Serial.println(heading);
  
 
 
  
} 
