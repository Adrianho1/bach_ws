#define csMPU 8
#define csRF24 10
#define magY IMU.getMagY_uT()
#define magX IMU.getMagX_uT()
#define magZ IMU.getMagZ_uT()

#include "MPU9250.h"

MPU9250 IMU(SPI,csMPU);
int status;
float heading;

void setup() {
  pinMode(csRF24, OUTPUT);      //setting Chip Select for nRF24
  digitalWrite(csRF24, HIGH);   //disabling nRF24
  
  // serial to display data
  Serial.begin(9600);
  while(!Serial) {}

  // start communication with IMU 
  status = IMU.begin();
  if (status < 0) {
    Serial.println("IMU initialization unsuccessful");
    Serial.println("Check IMU wiring or try cycling power");
    Serial.print("Status: ");
    Serial.println(status);
    while(1) {}
  }
}

void loop() {
  IMU.readSensor();
  Serial.print(magX);
  Serial.print("\t");
  Serial.print(magY);
  Serial.print("\t");
  Serial.print(magZ);
  Serial.print("\t");

 heading = /*90 - */atan2(magX,magY)*(180)/PI;
 if (heading < 0){
  heading = 360 + heading;
 }
  //else if (magY < 0) { heading = 270 - atan(magY/magZ)*(180/M_PI); }
  //else if (magY == 0 && magX < 0) { heading = 180; }
  //else if (magY == 0 && magX > 0) { heading = 0; }

  Serial.println(heading);
}
