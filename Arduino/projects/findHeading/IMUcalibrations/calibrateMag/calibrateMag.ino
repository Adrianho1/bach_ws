#include "MPU9250.h"

#define csMPU 8
#define csRF24 10

MPU9250 IMU(SPI,csMPU);
int status;

/* INCLUDE ABOVE THIS */

int result;
int runCalib = 1;
void setup() {

 pinMode(csRF24, OUTPUT);
 digitalWrite(csRF24, HIGH);

 Serial.begin(9600);
  while(!Serial) {};

 status = IMU.begin();
 if (status < 0) {
   Serial.println("IMU initialization unsuccessful");
   Serial.println("Check IMU wiring or try cycling power");
   Serial.print("Status: ");
   Serial.println(status);
   while(1) {}
 }
}

void loop() {
  Serial.println("About to start calibrating...");
  if(runCalib ==1){
    result = IMU.calibrateMag();
    runCalib = 0;
  }
  Serial.print("Calibration was: ");
  if (result > 0){
   Serial.println("SUCCESSFULL");
  }
  else {
   Serial.println("NOT SUCCESSFULL");
  }
  if(runCalib == 0){
    Serial.println("Done calibrating. Please disconnect");
    delay(2000);
  }
}
