/*
*Code #3 for the DRONE
*Wait for status request
*if OK; reads magnetometer(heading)
*gives status(heading) if OK
*/
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_LSM303_U.h>

/* Assign a unique ID to this sensor at the same time */
Adafruit_LSM303_Mag_Unified mag = Adafruit_LSM303_Mag_Unified(12345);

#define baud 9600
#define csNRF 10       //ChipSelect for nRF24
#define radioEnable 9  //ChipEable for nRF24

#define magY IMU.getMagY_uT()
#define magX IMU.getMagX_uT()
#define magZ IMU.getMagZ_uT()


RF24 radio(radioEnable,csNRF); // CE, CSN
const byte addresses[][6] = {"sNode","dNode"}; //Drone writes dNode. vice-versa

bool pendingRequest = 0;
//float heading;

void setup() {
 initSerial();// delete later. Initiates SerialCom.
 initLSM();   //Initiates the compass
 initNRF();   //Initiates the radio  
} //---End SETUP

void loop() {
  radio.startListening();
  delay(5); 
  
  if (radio.available()) {
    radio.read(&pendingRequest, sizeof(pendingRequest));
    delay(10); 
  }
  if (pendingRequest == 1){     //Reads heading and sends it
    respond();
  }  
} //-------End LOOP



/**********FUNCTIONS**********/
/****Setup***/
void initLSM(){
  if(!mag.begin()){
    /* There was a problem detecting the LSM303 ... check your connections */
    Serial.println("Ooops, no LSM303 detected ... Check your wiring!");
    while(1);
  }
}
void initSerial(){
  Serial.begin(baud);
}
void initNRF(){
  radio.begin();
  radio.openWritingPipe(addresses[1]);      //Write on dNode
  radio.openReadingPipe(1,addresses[0]);    //Listen to sNode
  radio.setPALevel(RF24_PA_MIN);            //Set min.TX-power
}

/***Actions***/
void respond(){
  radio.stopListening(); 
  
  sensors_event_t event; 
  mag.getEvent(&event);
  float heading = (atan2(event.magnetic.y,event.magnetic.x) * 180) / PI;

  if (heading < 0){
    heading = 360 + heading;
  }
  byte sendHdg = map(heading, 0,360,0,255);
  radio.write(&sendHdg, sizeof(sendHdg));
  pendingRequest = 0;
  //delay(50);
}

/*float getHeading(){
  //float heading;
  
  
 // heading = atan2(magY,magX)*(180)/PI;
  if (heading < 0){
  heading = 360 + heading;
  }
  return heading;
}*/
