/*
* Code #3 for the STATION
* Awaits prompt from ROS
* Prompts DRONE for status
* Receivs droneStatus (HDG) 
* Publishes 
*/

#include <ros.h>
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

#define baud 9600

RF24 radio(9,10); // CE, CSN
const byte addresses[][6] = {"sNode","dNode"};

const int requestButton = 2;
bool pendingRequest = 0;
byte heading;

/* --- Setup --- */
void setup() {
 initSERIAL();
 initNRF();
}

/* --- Code --- */
void loop() {
 
  pendingRequest = waitForRequest(requestButton); //Waits for user input

  heading = getHeading(pendingRequest); //sending if pendingRequest = 1
   

 
} 
//---End LOOP------------------------------------------------------------------|


bool waitForRequest(int button) {
  if (digitalRead(button)){             //Debounce
    delay(5);
    
    while(digitalRead(button)) {}       //Debounce
    return 1;
  }
  else {
    return 0;
  }
}


byte getHeading(bool x){
  if(x == 1){
    
    radio.stopListening();
    Serial.println("Sending...");
    radio.write(&pendingRequest, sizeof(pendingRequest));
    pendingRequest = 0;
    
    radio.startListening();
    delay(20);    //needs 20ms
    if (radio.available()) {
        int headingRx;
        radio.read(&headingRx, sizeof(headingRx));
        Serial.print("Drone HDG: ");
        Serial.println(headingRx);
        return headingRx;
    }
  }
}
/* --- Init's ---*/

void initSERIAL(){
  Serial.begin(baud);
}
void initNRF(){
  radio.begin();
  radio.openWritingPipe(addresses[0]);          //Write on sNode
  radio.openReadingPipe(1,addresses[1]);        //Listen on dNode
  radio.setPALevel(RF24_PA_MIN);                //Set min.TX-power
}
