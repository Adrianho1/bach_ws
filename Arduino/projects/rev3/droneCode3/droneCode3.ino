/*
*Code #3 for the DRONE
*Wait for status request
*if OK; reads magnetometer(heading)
*gives status(heading) if OK
*/
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

#define csMPU 8
#define csNRF 10

#define MPU 0;

RF24 radio(9,csNRF); // CE, CSN
const byte addresses[][6] = {"sNode","dNode"}; //Drone writes dNode. vice-versa

const int headingPin = A0;

bool pendingRequest = 0;
byte heading;

void setup() {
 initMPU();   //Initiates the compass
 initPOT();   //Initiates the pot-meter
 initNRF();   //Initiates the radio  
} //---End SETUP

void loop() {
  radio.startListening();
  delay(5); 
  
  if (radio.available()) {
    radio.read(&pendingRequest, sizeof(pendingRequest));
    delay(10); 
  }
  if (pendingRequest == 1){     //Reads heading and sends it
    respond();
  }  
} //-------End LOOP



/**********FUNCTIONS**********/
/****Setup***/
void initMPU(){
  pinMode(csMPU, OUTPUT);
  digitalWrite(csMPU, HIGH);
}
void initPOT(){
  pinMode(headingPin, INPUT);
}
void initNRF(){
  radio.begin();
  radio.openWritingPipe(addresses[1]);      //Write on dNode
  radio.openReadingPipe(1,addresses[0]);    //Listen to sNode
  radio.setPALevel(RF24_PA_MIN);            //Set min.TX-power
}

/***Actions***/
void respond(){
  radio.stopListening(); 
  heading = analogRead(headingPin)/4;
  radio.write(&heading, sizeof(heading));
  pendingRequest = 0;
  //delay(50);
}
