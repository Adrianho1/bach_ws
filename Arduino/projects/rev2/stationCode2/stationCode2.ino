/*
* Code # for the STATION
* Prompts for status
* Receivs status (heading) 
*/
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

RF24 radio(9,10); // CE, CSN

const byte addresses[][6] = {"sNode","dNode"};

const int requestButton = 2;
bool pendingRequest = 0;
byte headingRx;

void setup() {
  Serial.begin(9600);
  
  radio.begin();
  radio.openWritingPipe(addresses[0]);          //Write on sNode
  radio.openReadingPipe(1,addresses[1]);        //Listen on dNode
  radio.setPALevel(RF24_PA_MIN);                //Set min.TX-power
/*slett?-->*/  radio.startListening();
}
void loop() {
  if (digitalRead(requestButton)){      //Debounce
    pendingRequest = 1;
    Serial.print("Sending request");
    for (int i = 0; i <= 10; i++) {
      Serial.print(".");
      delay(10);
    }
  }

  if (pendingRequest){
    radio.stopListening();
    radio.write(&pendingRequest, sizeof(pendingRequest));
    Serial.println("DONE");
    pendingRequest = 0;
  }

  radio.startListening();
  if (radio.available()) {
    radio.read(&headingRx, sizeof(headingRx));
    Serial.print("Drone HDG: ");
    Serial.println(headingRx);
    delay(200);
  }
}
