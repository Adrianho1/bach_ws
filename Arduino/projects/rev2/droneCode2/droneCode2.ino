/*
*Code #2 for the DRONE
*Wait for status request
*gives status(heading) if OK
*/
#define csRF24 10
#define csMPU 8

#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

RF24 radio(9,csRF24); // CE, CSN

const byte addresses[][6] = {"sNode","dNode"}; //Drone writes dNode. vice-versa
const int headingPin = A0;
bool pendingRequest = 0;
byte heading;

void setup() {
  pinMode(csMPU, OUTPUT);       //Setup chipselect for compass
  digitalWrite(csMPU, HIGH);    //Disable compass
  pinMode(headingPin, INPUT);
  
  radio.begin();
  radio.openWritingPipe(addresses[1]);      //Write on dNode
  radio.openReadingPipe(1,addresses[0]);    //Listen to sNode
  radio.setPALevel(RF24_PA_MIN);            //Set min.TX-power
  
}

void loop() {
  digitalWrite(csMPU, HIGH);    //Disable compass
  radio.startListening();
  
  if (radio.available()) {
    radio.read(&pendingRequest, sizeof(pendingRequest));
    delay(5); 
  }
  if (pendingRequest == 1){     //Reads heading and sends it
    radio.stopListening(); 
    heading = analogRead(headingPin)/4;
    radio.write(&heading, sizeof(heading));
    pendingRequest =0;
    delay(5);
  }  
}
