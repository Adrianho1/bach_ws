/*
*Code #1 for the DRONE
*Simple TX of potValue(headingPin)
*/
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

RF24 radio(9,10); // CE, CSN

const byte address[6] = "00004";
const int headingPin = A0;

void setup() {
  //pinMode(headingPin, INPUT);
  
  radio.begin();
  radio.openWritingPipe(address);
  radio.setPALevel(RF24_PA_MIN);
  radio.stopListening();
}

void loop() {
//  const char text[] = "Hello World";
  byte heading = 123;
  heading = analogRead(headingPin)/4;
  radio.write(&heading, sizeof(heading));
  delay(1000);
}
