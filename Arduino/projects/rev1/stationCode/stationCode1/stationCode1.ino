/*
* Code #1 for the STATION
* Simple RX of a potValue (heading)
*/
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
RF24 radio(9,10); // CE, CSN
const byte address[6] = "00004";
void setup() {
  Serial.begin(9600);
  radio.begin();
  radio.openReadingPipe(0, address);
  radio.setPALevel(RF24_PA_MIN);
  radio.startListening();
}
void loop() {
  if (radio.available()) {
    byte headingRx;
    radio.read(&headingRx, sizeof(headingRx));
    Serial.println(headingRx);
  }
}
