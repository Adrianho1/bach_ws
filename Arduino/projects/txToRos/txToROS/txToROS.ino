#include <ros.h>
#include <std_msgs/UInt8.h>

#define headingPin A1
#define baud 9600
byte heading;

ros::NodeHandle nh;

std_msgs::UInt8 hdg_msg;
ros::Publisher drone_node("drone_node", &hdg_msg);

//char hello[13] = "hello world!";
//byte heading = 0;

void setup() {
  initSerial();
pinMode(headingPin, INPUT);
Serial.begin(baud);

nh.initNode();
nh.advertise(drone_node);

}

void loop() {
  heading = getHeading();
  hdg_msg.data = heading;
  Serial.println(heading);
  drone_node.publish( &hdg_msg );
  nh.spinOnce();
  delay(100);
  

}

byte getHeading(){
  return (analogRead(headingPin))/4;
}
