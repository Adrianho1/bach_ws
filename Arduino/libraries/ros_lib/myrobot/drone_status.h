#ifndef _ROS_myrobot_drone_status_h
#define _ROS_myrobot_drone_status_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace myrobot
{

  class drone_status : public ros::Msg
  {
    public:
      typedef bool _status_type;
      _status_type status;
      typedef uint8_t _heading_type;
      _heading_type heading;

    drone_status():
      status(0),
      heading(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_status;
      u_status.real = this->status;
      *(outbuffer + offset + 0) = (u_status.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->status);
      *(outbuffer + offset + 0) = (this->heading >> (8 * 0)) & 0xFF;
      offset += sizeof(this->heading);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_status;
      u_status.base = 0;
      u_status.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->status = u_status.real;
      offset += sizeof(this->status);
      this->heading =  ((uint8_t) (*(inbuffer + offset)));
      offset += sizeof(this->heading);
     return offset;
    }

    const char * getType(){ return "myrobot/drone_status"; };
    const char * getMD5(){ return "0835992c5281b666e3ea5bcc03c0f750"; };

  };

}
#endif
